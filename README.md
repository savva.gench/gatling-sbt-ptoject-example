# Steps for setup Load Testing on Gitlab CI

This Project is based on [workshop](https://gitlab.com/tinkoffperfworkshop)

Preconditions: 

```
git clone https://gitlab.com/savva.gench/gatling-sbt-ptoject-example.git
```

## 1. Run Test Application:
(more details about test application - [here](https://github.com/savvagen/django-profiling-poc))
``` 
$ cd gatling-sbt-ptoject-example/app 
$ docker-compose up -d
```

## 2. Run Metrics Services:
``` 
$ cd gatling-sbt-ptoject-example/app 
$ docker-compose up -d
```

## 3. Register Gitlab Runner:
``` 
$ docker exec -it gitlab-runner register \
  --non-interactive --url "https://gitlab.com/" --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
  --executor "docker" --docker-image alpine:latest --description "docker-runner" --tag-list "gatling-docker-runner" \
  --run-untagged="true" --locked="false" --access-level="not_protected"  --docker-network-mode="gatling-sandbox"

$ docker exec -it gitlab-runner cat /etc/gitlab-runner/config.toml

Expected output:

[[runners]]
  name = "gatling-docker-runner"
  url = "https://gitlab.com/"
  token = "<token>"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    network_mode = "gatling-sandbox"
    shm_size = 0
```


## 4 Running Simulation:
``` 
export GRAPHITE_HOST=localhost
export GRAPHITE_PORT=2003
export INFLUX_PREFIX=gatling
export BASE_URL=http://localhost:8001

sbt "gatling:testOnly com.example.simulations.LoadTestSimulation" 

```
