package com.example.cases

import java.util.Locale

import com.github.javafaker.Faker
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object Articles extends Articles {

  def getArticles() = {
    exec(http("GET /api/articles").get("/api/articles/")
      .check(status.is(200))
      .check(jsonPath("$[*]").count.gte(1))
    )
  }

  def getRandomArticle() = {
    exec(http("GET /api/articles/{id}").get("/api/articles/${sequentialInt}/")
      .check(status.is(200))
      .check(jsonPath("$.subject").ofType[String].in("Hello World"))
    )
  }

  def createArticle() = {
    exec(http("POST /api/articles").post("/api/articles/")
      .body(StringBody(
        """{
          |"title": "Test Article ${randomName}",
          |"subject": "Hello World",
          |"body": "<h1>Hello World Article</h1></p><h3>${randomName}</h3>",
          |"author": ${ID}
          |}""".stripMargin))
      .check(status.is(201))
      .check(jsonPath("$.subject").ofType[String].in("Hello World"))
    )
  }


}


class Articles {
  var faker = new Faker(new Locale("en_us"))
}