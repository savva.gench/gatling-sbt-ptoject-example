package com.example.cases

import java.util.Locale

import com.github.javafaker.Faker
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Authors extends Authors {

  def getAuthors() = {
    exec(http("GET /api/authors/").get("/api/authors/")
      .check(status.is(200))
      .check(jsonPath("$[*]").count.gte(1))
    )
  }

  def createAuthor() = {
    exec(http("POST /api/authors").post("/api/authors/")
      .body(StringBody(
        s"""{
          |"first_name": "${faker.name().firstName()}",
          |"last_name": "${faker.name().lastName()}",
          |"name": "${faker.name().username()}",
          |"email": "${faker.internet().emailAddress()}"
          |}""".stripMargin))
      .check(status.is(201))
      .check(jsonPath("$.id").ofType[Int].saveAs("authorId"))
    )
  }



}

class Authors {
  var faker = new Faker(new Locale("en_us"))
}
