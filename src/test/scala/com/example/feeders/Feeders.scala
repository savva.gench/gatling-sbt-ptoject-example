package com.example.feeders

import io.gatling.core.Predef._
import ru.tinkoff.gatling.feeders._

object Feeders {

  val randomName = RandomRangeStringFeeder("randomName", 10, 20, "qwerty12345")

  val sequentialInt = SequentialFeeder("sequentialInt", 10, 1)

  val userIds = csv("data/userIds.csv").eager.random

}
