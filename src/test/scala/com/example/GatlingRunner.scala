package com.example

import com.example.simulations.LoadTestSimulation
import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder

object GatlingRunner {

  def main(args: Array[String]): Unit = {

    // this is where you specify the class you want to run
    val simClass = classOf[LoadTestSimulation].getName

    val props = new GatlingPropertiesBuilder
    props.simulationClass(simClass)
    props.resourcesDirectory("src/main/resources")
    props.resultsDirectory("results")
    Gatling.fromMap(props.build)

  }

}
