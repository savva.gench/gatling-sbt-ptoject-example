package com.example.scenarios

import com.example.cases.{Articles, Authors}
import com.example.feeders.Feeders
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder

import scala.concurrent.duration._


object ArticlesScenario {
  def apply(): ScenarioBuilder = new ArticlesScenario().articlesScn
}

class ArticlesScenario {

  val articlesScn: ScenarioBuilder = scenario("My Scenario")
    .feed(Feeders.randomName)
    .feed(Feeders.sequentialInt)
    .feed(Feeders.userIds)
    .exec(Articles.getArticles())
    .pause(500 milliseconds)
    .exec(Articles.getRandomArticle())
    .pause(500 milliseconds)
    .exec(Authors.getAuthors())
    .randomSwitch(
      30.0 -> exec(Authors.createAuthor()),
      70.0 -> exec(Articles.createArticle())
    )
    .pause(500 milliseconds)

}
