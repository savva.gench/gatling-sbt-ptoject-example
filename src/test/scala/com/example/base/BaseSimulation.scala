package com.example.base

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder


class BaseSimulation extends Simulation {


  def getProperty(propertyName: String, defaultValue: String) = {
    Option(System.getenv(propertyName))
      .orElse(Option(System.getProperty(propertyName)))
      .getOrElse(defaultValue)
  }

  val httpProtocol: HttpProtocolBuilder = http
    //.baseUrl(baseUrl) // get "baseUrl" variable from simulation.conf
    .baseUrl(getProperty("BASE_URL", "http://localhost:8001"))
    .acceptCharsetHeader("utf-8")
    .contentTypeHeader("application/json;charset=utf-8")
    //.proxy(Proxy("localhost", 8888).httpsPort(8888))
    .disableWarmUp
    .disableCaching
    .disableFollowRedirect


}
