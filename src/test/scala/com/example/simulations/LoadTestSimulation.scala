package com.example.simulations

import com.example.base.BaseSimulation
import com.example.scenarios.ArticlesScenario
import io.gatling.core.Predef._
import ru.tinkoff.gatling.config.SimulationConfig._
import ru.tinkoff.gatling.influxdb.Annotations

class LoadTestSimulation extends BaseSimulation with Annotations {

  before {
    println("Starting Load Test")
  }

  setUp(
    ArticlesScenario().inject(
      rampUsersPerSec(0) to intensity.toInt during rampDuration, //разгон
      constantUsersPerSec(intensity.toInt) during stageDuration //полка
    )
  ).protocols(httpProtocol)
    .maxDuration(testDuration)


}
