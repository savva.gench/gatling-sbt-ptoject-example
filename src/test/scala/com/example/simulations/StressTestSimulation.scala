package com.example.simulations

import com.example.base.BaseSimulation
import com.example.scenarios.ArticlesScenario
import io.gatling.core.Predef._
import ru.tinkoff.gatling.config.SimulationConfig._
import ru.tinkoff.gatling.influxdb.Annotations

class StressTestSimulation extends BaseSimulation with Annotations {

  before {
    println("Starting Stress Test")
  }

  setUp(
    ArticlesScenario().inject(
      incrementUsersPerSec((intensity / stagesNumber).toInt) // интенсивность на ступень
        .times(stagesNumber) // Количество ступеней
        .eachLevelLasting(stageDuration) // Длительность полки
        .separatedByRampsLasting(rampDuration) // Длительность разгона
        .startingFrom(0) // Начало нагрузки с
    )
  ).protocols(httpProtocol)
    .maxDuration(testDuration)

}
