version: '3.7'
services:

  influxdb:
    image: influxdb:1.8
    container_name: influxdb
    restart: unless-stopped
    environment:
      - INFLUXDB_CONFIG_PATH=/tmp/influxdb/influxdb.conf
      - INFLUXDB_DB=testdb
    volumes:
      - ./config/influxdb:/tmp/influxdb
      - influx-data:/var/lib/influxdb
    ports:
      - 8086:8086
      - 2003:2003

  loki:
    image: grafana/loki:1.5.0
    container_name: loki
    restart: unless-stopped
    command: -config.file=/etc/loki/local-config.yaml
    ports:
      - 3100:3100
    healthcheck:
      test: ["CMD", "wget", "--spider", "-S", "http://localhost:3100/ready"]
      interval: 1m
      timeout: 10s
      retries: 3
      start_period: 1m

  grafana:
    image: grafana/grafana:7.2.1
    container_name: grafana
    restart: unless-stopped
    depends_on:
      - influxdb
    environment:
      - GF_PATHS_PROVISIONING=/etc/grafana/provisioning
      - GF_SECURITY_ADMIN_PASSWORD=admin
      - GF_INSTALL_PLUGINS=michaeldmoore-multistat-panel,grafana-piechart-panel
    volumes:
      - grafana-data:/var/lib/grafana
      - ./config/grafana/datasources:/etc/grafana/provisioning/datasources
      - ./config/grafana/dashboards:/etc/grafana/provisioning/dashboards
    ports:
      - 3000:3000

  cadvisor:
    image: gcr.io/google-containers/cadvisor:v0.34.0
    container_name: cadvisor
    restart: unless-stopped
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
    ports:
      - 8080:8080

  prometheus:
    image: prom/prometheus:v2.22.0
    container_name: prometheus
    restart: unless-stopped
    command: --config.file=/etc/prometheus/prometheus.yml
    volumes:
      - ./config/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml:ro
    ports:
      - 9090:9090
    depends_on:
      - cadvisor

  gitlab-runner:
    image: gitlab/gitlab-runner:v13.4.1
    container_name: gitlab-runner
    restart: unless-stopped
    volumes:
      - gitlab-runner-config:/etc/gitlab-runner
      - /var/run/docker.sock:/var/run/docker.sock

  vector:
    image: timberio/vector:0.10.0-alpine
    container_name: vector
    restart: unless-stopped
    volumes:
      - ./config/vector/vector.toml:/etc/vector/vector.toml:ro
      - /var/run/docker.sock:/var/run/docker.sock
    depends_on:
      - gitlab-runner
      - loki

  portainer:
    image: portainer/portainer:1.24.1
    container_name: portainer
    restart: unless-stopped
    command: -H unix:///var/run/docker.sock
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - portainer-data:/data
    ports:
      - 9000:9000
      - 8000:8000

networks:
  default:
    name: gatling-sandbox

volumes:
  influx-data:
    name: influx-data
  grafana-data:
    name: grafana-data
  portainer-data:
    name: portainer-data
  gitlab-runner-config:
    name: gitlab-runner-config
